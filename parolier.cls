%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Parolier — by ChENSon Douce %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{parolier}

\LoadClass[7pt]{article}

\RequirePackage{verse}
\RequirePackage{multicol}
\RequirePackage{titlesec}
\RequirePackage{fontspec}
\RequirePackage{lscape}
\RequirePackage{hyperref}
\RequirePackage{xcolor}
\RequirePackage{tocloft}
\RequirePackage{fancybox}

%%%% Hyperref

\definecolor{link_blue}{RGB}{0,0,97}
\hypersetup{
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=link_blue,   % color of internal links (change box color with linkbordercolor)
    urlcolor=link_blue     % color of external links
}

%%%% Table of contents

\setlength\cftparskip{0pt}
\setlength\cftbeforesecskip{4pt}
%\setlength\cftaftertoctitleskip{2pt}

%%%% Polices

\newfontfamily\sectionfont[Path=fonts/,
                           BoldFont=URWBookman-DemiItalic.otf]
                          {URWBookman-DemiItalic.otf}

%%%% Modifications générales

\titleformat*{\section}{\LARGE\bfseries\sectionfont\centering}
\titlespacing*{\section}{0pt}{*0}{*5}
\setlength{\vleftskip}{2mm}

%%%% Page de garde

\renewcommand*{\maketitle}{%
    \begin{titlepage}
        \centering\hspace{0pt}

        \vfill{}

        {\LARGE \@title \par}
        \vspace{2em}
        {\Large\itshape\@author \par}

        \vfill{}\hspace{0pt}\vfill{}
        % ^^^ Dirty way to have fractional vertical alignment: title is placed
        % at 1/3 of the page
    \end{titlepage}
}

%%%% Inclusion de pages de chansons

\newcommand{\internalSetupchanson}{}
\newcommand{\internalCleanupchanson}{}

\newenvironment{internalImagedroite}{}{}

\newlength{\saveleftmargini}
\newenvironment{chanson}{
    % Change *this* verse left margin
    \setlength{\saveleftmargini}{\leftmargini}
    \setlength{\leftmargini}{1em}

    \internalSetupchanson{}
    \begin{internalImagedroite}
    \begin{verse}
}{
    \end{verse}
    \end{internalImagedroite}
    \internalCleanupchanson{}

    \setlength{\leftmargini}{\saveleftmargini}
}

\newcommand{\internalNouvellechanson}{
    \renewenvironment{internalImagedroite}{}{}
}

\newcommand{\inputchanson}[1]{\chansonunecol{#1}} %< Alias, pour les flemmard·e·s

\newcommand{\chansonunecol}[1]{ %< Sur une colonne
    \internalNouvellechanson{}
    \input{chansons/#1}
    \pagebreak
}

\newcommand{\chansondeuxcol}[1]{ %< Sur deux colonnes
    \internalNouvellechanson{}
    \renewcommand{\internalSetupchanson}{
        \begin{multicols}{2}
    }
    \renewcommand{\internalCleanupchanson}{
        \end{multicols}
    }

    \input{chansons/#1}
    \pagebreak

    \renewcommand{\internalSetupchanson}{}
    \renewcommand{\internalCleanupchanson}{}
}


%%%% Formattage des chansons

\setcounter{secnumdepth}{0}

\newcommand{\titre}[1]{
    \section{#1}
}

\newcommand{\tonalite}[1]{
    % Attention : hackish as hell.
    % Les valeurs numériques sont fine-tuned pour que les espacements soient
    % les bons.
    \vspace{-4em}\hfill\Ovalbox{\texttt{\textit{#1}}}
    \vspace{2.6em}
}

\newcommand{\structure}[1]{
    \vspace{0.6em}
    \textbf{[#1]}
    \vspace{0.6em}
}

\newcommand{\structuredef}[1]{
    \hspace{-0.5em}\textbf{#1} \\*
}

\newcommand{\indication}[1]{
    \hspace{1em}--~\textit{#1}~--
}

\newcommand{\voixoff}[1]{
    {(\small\textit{#1})}
}

\newcommand{\indicationligne}[1]{
    \flagverse{\textbf{[\texttt{#1}]}}
}

%%%% Fluff

\newcommand{\imagechanson}[1]{
    \vspace{1em}
    \begin{center}
        \includegraphics[
            height=\dimexpr\textheight-\pagetotal-2em\relax,
            width=0.9\textwidth,
            keepaspectratio]{imgs/#1}
    \end{center}
}

\newcommand{\imagedroite}[2]{  %< À mettre AVANT \begin{chanson}
    \renewenvironment{internalImagedroite}{
        \begin{minipage}[c]{\dimexpr\textwidth-#2-2em}
    }
    {
        \end{minipage}\hfill\begin{minipage}{#2}
            {
                \centering
                \includegraphics[width=\textwidth]{imgs/#1}
            }
        \end{minipage}
        \vfill
    }
}
